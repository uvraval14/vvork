# VVork



## Name
VVork

## Description
This Gitlab project contains resources for VVork fans.

## Contents
This project is laid out as follows...

### Courses
This directory contains files related to courses delivered by [VMware Education](https://www.vmware.com/education).

### YouTube
This directory contains files related to videos on the [VVork YouTube channel](https://youtube.com/@vvork_info).
